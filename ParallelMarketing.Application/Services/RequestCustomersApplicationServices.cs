﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using ParallelMarketing.Application.Interfaces;
using ParallelMarketing.Application.Dto;
using ParallelMarketing.Domain.Models;
using ParallelMarketing.Domain.Interfaces.Request;

namespace ParallelMarketing.Application.Services
{
    public class RequestCustomersApplicationServices : IRequestCustomersApplications
    {
        //private readonly ILogger _logger;
        private readonly IMapper _mapper;
        private readonly IRequestCustomersDomain _requestCustomersDomain;

        public RequestCustomersApplicationServices(IRequestCustomersDomain requestCustomersDomain, IMapper mapper)
        {
            //_logger = logger;
            _requestCustomersDomain = requestCustomersDomain;
            _mapper = mapper;
        }

        public async Task<CustomersDto> RequestHttpItau<T>(T model)
        {
            //_logger.LogInformation("Api Itaú - ");
            var customersMap = _mapper.Map<Customers>(model);
            var result = await _requestCustomersDomain.SendMarketingItau<Customers>(customersMap);
            var customersDtoMap = _mapper.Map<CustomersDto>(result);
            //_logger.LogInformation("Api Itaú - ");
            return customersDtoMap;
            
        }


    }
}
