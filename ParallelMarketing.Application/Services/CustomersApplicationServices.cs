﻿using ParallelMarketing.Application.Interfaces;
using ParallelMarketing.Domain.Interfaces;
using ParallelMarketing.Domain.Models;

namespace ParallelMarketing.Application.Services
{
    public class CustomersApplicationServices<T> : ICustomersApplicationServices<T> where T : class
    {
        private readonly IUnitOfWork _unitOfWork;

        public CustomersApplicationServices(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task Add<T>(T model)
        {
            var entity = model as Customers;
            await _unitOfWork.CustomersRepository.AddCustomers(entity);

        }
    }
}
