﻿using AutoMapper;
using ParallelMarketing.Application.Dto;
using ParallelMarketing.Domain.Models;

namespace ParallelMarketing.Application.Mapper
{
    public class ProfileMapper : Profile
    {
        public ProfileMapper()
        {
            CreateMap<CustomersDto, Customers>();
            CreateMap<Customers, CustomersDto>();
        }
    }
}
