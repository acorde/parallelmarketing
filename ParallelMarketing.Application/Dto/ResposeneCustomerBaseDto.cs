﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParallelMarketing.Application.Dto
{
    public abstract class ResposeneCustomerBaseDto
    {
        public int? Success { get; set; }
        public string? ResultMessage { get; set; }
        public string? Type { get; set; }
        public string? Title { get; set; }
        public int? Status { get; set; }
        public string? TraceId { get; set; }
    }
}
