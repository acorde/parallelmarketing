﻿using System.Text.Json.Serialization;

namespace ParallelMarketing.Application.Dto
{
    public class AddressDto
    {
        public int Id { get; set; }
        [JsonPropertyName("UfParametro")]
        public string? UfParametro { get; set; }
        public string? Logradouro { get; set; }
        public string? Numero { get; set; }
        public string? Complemento { get; set; }
        public string? CEP { get; set; }
        public string? Bairro { get; set; }
        public string? Municipio { get; set; }
        public string? UF { get; set; }
        public string? CodigoIbge { get; set; }
        public int CustomersId { get; set; }
    }
}
