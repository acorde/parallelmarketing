﻿using ParallelMarketing.Application.Dto;

namespace ParallelMarketing.Application.Interfaces
{
    public interface IRequestCustomersApplications
    {
        
        Task<CustomersDto> RequestHttpItau<T>(T model);
    }
}
