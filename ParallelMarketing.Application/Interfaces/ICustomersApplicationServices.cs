﻿namespace ParallelMarketing.Application.Interfaces
{
    public interface ICustomersApplicationServices<T> where T : class
    {
        Task Add<T>(T model);
    }
}
