﻿using ParallelMarketing.Domain.Interfaces.MQ;

namespace ParallelMarketing.Application.MQ.Customers
{
    public class MQCustomersPublisher : IMQCustomersPublisher
    {
        private readonly IMQCustomersPublisher _publisher;

        public MQCustomersPublisher(IMQCustomersPublisher publisher)
        {
            _publisher = publisher;
        }
        public async Task PublisherMessageBradesco<T>(T message)
        {
            await _publisher.PublisherMessageBradesco(message);
        }

        public async Task PublisherMessageItau<T>(T message)
        {
            await _publisher.PublisherMessageItau(message);
        }

        public async Task PublisherMessageNubank<T>(T message)
        {
            await _publisher.PublisherMessageNubank(message);
        }

        public async Task PublisherMessageSantander<T>(T message)
        {
            await _publisher.PublisherMessageSantander(message);
        }
    }
}
