﻿using AutoMapper;
using MediatR;
using ParallelMarketing.Application.Dto;
using ParallelMarketing.Domain.Interfaces;
using ParallelMarketing.Domain.Models;

namespace ParallelMarketing.Application.Customer.Command
{
    public class CreateCustomersCommandHandler : IRequestHandler<CreateCustomersCommand, CustomersDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CreateCustomersCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<CustomersDto> Handle(CreateCustomersCommand request, CancellationToken cancellationToken)
        {
            var customerDto = new CustomersDto(request.CodigoHtml, request.CodigoInterno, request.CnpjParametro, request.CnpjConsultado,
                                          request.NumeroInscricao, request.NomeEmpresarial, request.InscricaoEstadual, request.ClientName,  
                                          request?.AddressDto);

            var customerMap = _mapper.Map<CustomersDto, Customers>(customerDto);
            await _unitOfWork.CustomersRepository.AddCustomers(customerMap);
            await _unitOfWork.CommitAsync();

            return customerDto;
        }
    }
}
