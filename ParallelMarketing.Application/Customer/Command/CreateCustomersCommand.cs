﻿using MediatR;
using ParallelMarketing.Application.Dto;
using ParallelMarketing.Domain.Models;

namespace ParallelMarketing.Application.Customer.Command
{
    public class CreateCustomersCommand : IRequest<CustomersDto>
    {
        //public int? success { get; set; }
        //public string? resultMessage { get; set; }
        //public string? type { get; set; }
        //public string? title { get; set; }
        //public int? status { get; set; }
        //public string? traceId { get; set; }
        public string? CodigoHtml { get; set; }
        public string? CodigoInterno { get; set; }
        public string? CnpjParametro { get; set; }
        public string? CnpjConsultado { get; set; }
        public string? NumeroInscricao { get; set; }
        public string? NomeEmpresarial { get; set; }
        public string? InscricaoEstadual { get; set; }
        public string? ClientName { get; set; }
        public AddressDto? AddressDto { get; set; }        
    }
}
