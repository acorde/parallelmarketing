﻿using Hangfire;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ParallelMarketing.Domain.Interfaces.MQ;

namespace ParallelMarketing.Services.Hangfire
{
    public class HFSchedullerServices : IHostedService
    {
        private readonly IMQCustomersConsumer _hfCustomersConsumerDomain;
        private readonly ILogger<HFSchedullerServices> _logger;

        public HFSchedullerServices(IMQCustomersConsumer hfCustomersConsumerDomain, ILogger<HFSchedullerServices> logger)
        {
            _hfCustomersConsumerDomain = hfCustomersConsumerDomain;
            _logger = logger;
        }
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await AgendarFilas();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        private async Task AgendarFilas()
        {
            await Task.Run(() =>
            {
                //RecurringJob.AddOrUpdate("std-customers", () => ConsumerMessageSantander(), MinuteInterval(1));
                RecurringJob.AddOrUpdate("customers", () => ConsumerMessageItau(), MinuteInterval(1));
                //RecurringJob.AddOrUpdate("nbk-customers", () => ConsumerMessageNubank(), MinuteInterval(1));
                //RecurringJob.AddOrUpdate("bdo-customers", () => ConsumerMessageBradesco(), MinuteInterval(1));
            });
        }

        public static string MinuteInterval(int interval)
        {
            return $"*/{interval} * * * *";
        }

        public async Task ConsumerMessageSantander()
        {
            try
            {
                await _hfCustomersConsumerDomain.ConsumerMessageSantander();
            }
            catch (Exception ex)
            {
                _logger.LogError($"ERRO: {ex.Message}.");
            }

        }
        public async Task ConsumerMessageItau()
        {
            try
            {
                await _hfCustomersConsumerDomain.ConsumerMessageItau();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Hangfire - Schedule: {ex.Message}.");
            }
        }

        public async Task ConsumerMessageNubank()
        {
            try
            {
                await _hfCustomersConsumerDomain.ConsumerMessageNubank();
            }
            catch (Exception ex)
            {
                _logger.LogError($"ERRO: {ex.Message}.");
            }
        }

        public async Task ConsumerMessageBradesco()
        {
            try
            {
                await _hfCustomersConsumerDomain.ConsumerMessageBradesco();
            }
            catch (Exception ex)
            {
                _logger.LogError($"ERRO: {ex.Message}.");
            }
        }
    }
}
