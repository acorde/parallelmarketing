﻿using ParallelMarketing.Domain.Models;

namespace ParallelMarketing.Domain.Interfaces.Request
{
    public interface IRequestCustomersDomain
    {
        public Task<Customers> SendMarketingItau<T>(T model);
    }
}
