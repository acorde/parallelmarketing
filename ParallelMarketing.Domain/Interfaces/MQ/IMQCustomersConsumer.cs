﻿namespace ParallelMarketing.Domain.Interfaces.MQ
{
    public interface IMQCustomersConsumer
    {
        Task ConsumerMessageNubank();
        Task ConsumerMessageItau();
        Task ConsumerMessageBradesco();
        Task ConsumerMessageSantander();
    }
}
