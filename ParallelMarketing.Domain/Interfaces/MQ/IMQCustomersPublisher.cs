﻿namespace ParallelMarketing.Domain.Interfaces.MQ
{
    public interface IMQCustomersPublisher
    {
        Task PublisherMessageNubank<T>(T message);
        Task PublisherMessageItau<T>(T message);
        Task PublisherMessageBradesco<T>(T message);
        Task PublisherMessageSantander<T>(T message);
    }
}
