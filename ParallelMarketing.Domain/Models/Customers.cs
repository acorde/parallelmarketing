﻿namespace ParallelMarketing.Domain.Models
{
    public class Customers
    {
        public Customers()
        {
            Address = new Address();
        }
        public int Id { get; set; }
        public string? CodigoHtml { get; set; }
        public string? CodigoInterno { get; set; }
        public string? CnpjParametro { get; set; }
        public string? CnpjConsultado { get; set; }
        public string? NumeroInscricao { get; set; }
        public string? NomeEmpresarial { get; set; }
        public string? InscricaoEstadual { get; set; }
        public string? ClientName {  get; set; }
        public Address Address { get; set; }

        public Customers( string? codigoHtml, string? codigoInterno, string? cnpjParametro, string? cnpjConsultado
            , string? numeroInscricao, string? nomeEmpresarial, string? inscricaoEstadual, string? clienteName, Address address)
        {

            CodigoHtml = codigoHtml;
            CodigoInterno = codigoInterno;
            CnpjParametro = cnpjParametro;
            CnpjConsultado = cnpjConsultado;
            NumeroInscricao = numeroInscricao;
            NomeEmpresarial = nomeEmpresarial;
            InscricaoEstadual = inscricaoEstadual;
            ClientName = clienteName;
            Address = address;
        }

        //public Customers(string? firstName, string? lastName, string? gender, string? email, bool? isActive)
        //{
        //    ValidateDomain(firstName, lastName, gender, email, isActive);
        //}

        //[JsonConstructor] //Informa ao serializdor qual construtor irá utilizar no momento da Serialização JSON
        //public Customers(int id, string? firstName, string? lastName, string? gender, string? email, bool? isActive)
        //{
        //    DomainValidation.When(id < 0, "Invalid Id Value");
        //    Id = id;
        //    ValidateDomain(firstName, lastName, gender, email, isActive);
        //}

        //public void Update(string? firstName, string? lastName, string? gender, string? email, bool? isActive)
        //{
        //    ValidateDomain(firstName, lastName, gender, email, isActive);
        //}

        //private void ValidateDomain(string? firstName, string? lastName, string? gender, string? email, bool? isActive)
        //{
        //    //DomainValidation.When(string.IsNullOrEmpty(firstName), "Invalid FirstName. FirstName is required");
        //    //DomainValidation.When(firstName.Length < 3, "Invalid FirstName, too short minimun 3 characteres");

        //    //DomainValidation.When(string.IsNullOrEmpty(lastName), "Invalid lastName. lastName is required");
        //    //DomainValidation.When(lastName.Length < 3, "Invalid lastName, too short minimun 3 characteres");

        //    //DomainValidation.When(string.IsNullOrEmpty(gender), "Invalid gender. gender is required");

        //    //DomainValidation.When(email?.Length > 250, "Invalid Email, too long maximun 250 characteres");
        //    //DomainValidation.When(email?.Length < 3, "Invalid Email, too long minimun 3 characteres");

        //    //DomainValidation.When(!IsActive.HasValue, "Must deife activity");

        //    //FirstName = firstName;
        //    //LastName = lastName;
        //    //Gender = gender;
        //    //Email = email;
        //    //IsActive = isActive;
        //}

    }

}
