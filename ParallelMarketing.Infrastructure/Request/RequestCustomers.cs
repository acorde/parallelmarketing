﻿using Microsoft.Extensions.Logging;
using ParallelMarketing.Domain.Interfaces.Request;
using ParallelMarketing.Domain.Models;
using System.Text;
using System.Text.Json;

namespace ParallelMarketing.Infrastructure.Request
{
    public class RequestCustomers : IRequestCustomersDomain
    {
        private readonly ILogger<RequestCustomers> _logger;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly JsonSerializerOptions _options;
        private readonly SettingsConfiguration _configuration;

        public RequestCustomers(ILogger<RequestCustomers> logger, IHttpClientFactory httpClientFactory, SettingsConfiguration configuration)
        {
            _logger = logger;
            _httpClientFactory = httpClientFactory;
            _options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            _configuration = configuration;
        }

        public async Task<Customers> SendMarketingItau<T>(T customer)
        {
            Customers? _customer = new Customers();
            try
            {
                _logger.LogInformation($"{nameof(T)} - Iniciando a requisição Http Post");
                var _httpClient = _httpClientFactory.CreateClient("parallel");
                var _content = new StringContent(JsonSerializer.Serialize(customer), Encoding.UTF8, "application/json");
                var _uri = _configuration?.Parameters?.Uri;

                using var _httpResponse = await _httpClient.PostAsync(_uri, _content);
                var _result = await _httpResponse.Content.ReadAsStringAsync();
                _customer = JsonSerializer.Deserialize<Customers>(_result, _options);
                if (_httpResponse.IsSuccessStatusCode)
                {
                    _logger.LogInformation($"Sucesso");
                    return _customer;
                }
                else
                {
                    _logger.LogWarning($"Cuidadp");
                    return _customer;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(T)} - {ex.Message}");
                return _customer;
            }
        }

    }
}
