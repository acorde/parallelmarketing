﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ParallelMarketing.Domain.Models;

namespace ParallelMarketing.Infrastructure.ModelsConfiguration
{
    public class CustomersConfiguration : IEntityTypeConfiguration<Customers>
    {
        public void Configure(EntityTypeBuilder<Customers> builder)
        {
            builder.HasKey(x => x.Id);
            //builder.HasOne(m => m.Address).WithOne(m => m.Customers);
            builder.Property(m => m.CodigoHtml);
            builder.Property(m => m.CodigoInterno);
            builder.Property(m => m.CnpjParametro);
            builder.Property(m => m.CnpjConsultado);
            builder.Property(m => m.NumeroInscricao);
            builder.Property(m => m.NomeEmpresarial);
            builder.Property(m => m.InscricaoEstadual);
            
            //builder.HasData(
            //    new Customers(1, "Maria", "Jopli", "feminino", "janes@email.com", true),
            //    new Customers(2, "Elvis", "Presley", "masculino", "elvis@email.com", true));
        }
    }
}
