﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParallelMarketing.Infrastructure
{
    public sealed class Parameters
    {
        public bool? IsProduction { get; set; }
        public string Uri { get; set; }
    }
}
