﻿using ParallelMarketing.Domain.Interfaces;
using ParallelMarketing.Infrastructure.Context;

namespace ParallelMarketing.Infrastructure.Repository
{
    public class UnitOfWorkRepository : IUnitOfWork, IDisposable
    {
        private ICustomersRepository? _customerRepo;
        private readonly AppDbContext _context;

        public UnitOfWorkRepository(AppDbContext context)
        {
            _context = context;
        }

        public ICustomersRepository CustomersRepository
        {
            get
            {
                return _customerRepo = _customerRepo ??
                    new CustomersRepository(_context);
            }
        }

        public async Task CommitAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
