﻿using Microsoft.EntityFrameworkCore;
using ParallelMarketing.Domain.Interfaces;
using ParallelMarketing.Domain.Models;
using ParallelMarketing.Infrastructure.Context;

namespace ParallelMarketing.Infrastructure.Repository
{
    public class CustomersRepository : ICustomersRepository
    {
        protected readonly AppDbContext _db;

        public CustomersRepository(AppDbContext db)
        {
            _db = db;
        }

        public async Task<Customers> GetCustomersById(int id)
        {
            var member = await _db.Customers.FindAsync(id);
            if (member is null) throw new InvalidOperationException("Member not found");

            return member;
        }

        public async Task<Customers> AddCustomers(Customers customer)
        {
            if (customer is null) throw new ArgumentNullException(nameof(customer));
            await _db.Customers.AddAsync(customer);
            await _db.SaveChangesAsync();
            return customer;
        }

        public async Task<IEnumerable<Customers>> GetCustomers()
        {
            var customer = await _db.Customers.ToListAsync();
            return customer ?? Enumerable.Empty<Customers>();
        }

        public void UpdateCustomers(Customers customer)
        {
            if (customer is null) throw new ArgumentNullException(nameof(customer));
            _db.Customers.Update(customer);

        }

        public async Task<Customers> DeleteCustomers(int id)
        {
            var customer = await GetCustomersById(id);
            if (customer is null) throw new InvalidOperationException($"{nameof(customer)} not found");
            _db.Customers.Remove(customer);
            return customer;
        }

    }
}
