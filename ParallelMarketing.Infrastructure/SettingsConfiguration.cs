﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParallelMarketing.Infrastructure
{
    public sealed class SettingsConfiguration
    {
        public bool? Log { get; set; }
        public string? ConnectionStringId { get; set; }
        public Parameters? Parameters { get; set; }
    }
}
