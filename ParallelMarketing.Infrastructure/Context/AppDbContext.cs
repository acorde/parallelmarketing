﻿using Microsoft.EntityFrameworkCore;
using ParallelMarketing.Domain.Models;
using ParallelMarketing.Infrastructure.ModelsConfiguration;

namespace ParallelMarketing.Infrastructure.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        public DbSet<Customers> Customers { get; set; }
        public DbSet<Address> Address { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new CustomersConfiguration());
            builder.ApplyConfiguration(new AddressConfiguration());
        }
    }
}
