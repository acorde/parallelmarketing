﻿using Microsoft.Extensions.Logging;
using ParallelMarketing.Domain.Interfaces.MQ;
using RabbitMQ.Client;
using System.Security;
using System.Text;
using System.Text.Json;
using System.Threading.Channels;

namespace ParallelMarketing.Infrastructure.MQ
{
    public class MQCustomersPublisher : IMQCustomersPublisher
    {
        private readonly ILogger<MQCustomersPublisher> _logger;

        public MQCustomersPublisher(ILogger<MQCustomersPublisher> logger)
        {
            _logger = logger;
        }

        public async Task PublisherMessageItau<T>(T message)
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory { HostName = "localhost" };
                using (var connection = factory.CreateConnection())
                {
                    var channel = CreateChannel(connection);
                    //BuildPublishers(channel1, "ita-customers", "Produtor A", message);
                    channel.ExchangeDeclare("DeadLetterQueue", ExchangeType.Fanout);
                    channel.QueueDeclare(queue: "DeadLetterQueue", durable: true, exclusive: false, autoDelete: false, arguments: null);
                    channel.QueueBind("DeadLetterQueue", "DeadLetterQueue", "");

                    var args = new Dictionary<string, object>()
                        {
                            { "x-dead-letter-exchange", "DeadLetterQueue" }
                        };

                    channel.QueueDeclare(queue: "customers", durable: true, exclusive: false, autoDelete: false, arguments: args);
                    var json = JsonSerializer.Serialize(message);
                    var body = Encoding.UTF8.GetBytes(json);
                    channel.BasicPublish(exchange: string.Empty, routingKey: "customers", basicProperties: null, body: body);
                };
            });
        }

        public static IModel CreateChannel(IConnection connection)
        {
            var channel = connection.CreateModel();
            return channel;
        }

        public static void BuildPublishers(IModel channel, string queueName, string publisherName, object? message)
        {
            Task.Run(() =>
            {
                channel.ExchangeDeclare("DeadLetterQueue", ExchangeType.Fanout);
                channel.QueueDeclare(queue: "DeadLetterQueue", durable: true, exclusive: false, autoDelete: false, arguments: null);
                channel.QueueBind("DeadLetterQueue", "DeadLetterQueue", "");

                var args = new Dictionary<string, object>()
                        {
                            { "x-dead-letter-exchange", "DeadLetterQueue" }
                        };

                channel.QueueDeclare(queue: "customers", durable: true, exclusive: false, autoDelete: false, arguments: args);
                var json = JsonSerializer.Serialize(message);
                var body = Encoding.UTF8.GetBytes(json);
                channel.BasicPublish(exchange: string.Empty, routingKey: "customers", basicProperties: null, body: body);
            });
        }

        public async Task PublisherMessageBradesco<T>(T message)
        {
            await Task.Run(() =>
            {
                try
                {
                    var factory = new ConnectionFactory() { HostName = "localhost" };
                    using var connection = factory.CreateConnection();
                    using var channel = connection.CreateModel();

                    //channel.ExchangeDeclare("bdo-customers-exh", "fanout");
                    //channel.QueueDeclare("bdo-customers-dlq", true, false, false, null);
                    //channel.QueueBind("bdo-customers-dlq", "bdo-customers-exh", "");

                    //var args = new Dictionary<string, object>()
                    //{
                    //    { "x-dead-letter-exchange", "bdo-customers-exh" }
                    //};

                    channel.QueueDeclare(queue: "bdo-customers", durable: true, exclusive: false, autoDelete: false, arguments: null);
                    var json = JsonSerializer.Serialize(message);
                    var body = Encoding.UTF8.GetBytes(json);
                    channel.BasicPublish(exchange: string.Empty, routingKey: "bdo-customers", basicProperties: null, body: body);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Ocorreu um erro na classe : {nameof(PublisherMessageBradesco)} : {ex.Message}.");
                }

            });
        }

        public async Task PublisherMessageSantander<T>(T message)
        {
            await Task.Run(() =>
            {
                try
                {
                    var factory = new ConnectionFactory { HostName = "localhost" };
                    using var connection = factory.CreateConnection();
                    using IModel channel = connection.CreateModel();

                    //channel.ExchangeDeclare("std.customers.exc", ExchangeType.Fanout);
                    //channel.QueueDeclare("std.customers.dlq", true, false, false, null);
                    //channel.QueueBind("std.customers.dlq", "std.customers.exc", "");

                    //var args = new Dictionary<string, object>()
                    //    {
                    //        { "x-dead-letter-exchange", "std.customers.exc" }
                    //    };

                    channel.QueueDeclare(queue: "std-customers", durable: true, exclusive: false, autoDelete: false, arguments: null);
                    var json = JsonSerializer.Serialize(message);
                    var body = Encoding.UTF8.GetBytes(json);
                    channel.BasicPublish(exchange: string.Empty, routingKey: "std-customers", basicProperties: null, body: body);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Ocorreu um erro na classe : {nameof(PublisherMessageSantander)} : {ex.Message}.");

                }

            });
        }

        public async Task PublisherMessageNubank<T>(T message)
        {
            var factory = new ConnectionFactory { HostName = "localhost" };
            using var connection = factory.CreateConnection();
            using (var channel = connection.CreateModel())
            {
                //channel1.ExchangeDeclare("nbk-customers-exh", "fanout");
                //channel1.QueueDeclare("nbk-customers-dlq", true, false, false, null);
                //channel1.QueueBind("nbk-customers-dlq", "nbk-customers-exh", "");

                //var args = new Dictionary<string, object>()
                //    {
                //        { "x-dead-letter-exchange", "nbk-customers-exh" }
                //    };

                channel.QueueDeclare(queue: "nbk-customers", durable: true, exclusive: false, autoDelete: false, arguments: null);
                var json = JsonSerializer.Serialize(message);
                var body = Encoding.UTF8.GetBytes(json);
                channel.BasicPublish(exchange: string.Empty, routingKey: "nbk-customers", basicProperties: null, body: body);
            }
        }



    }
}
