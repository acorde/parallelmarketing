﻿using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using Microsoft.Extensions.Logging;
using System.Text;
using ParallelMarketing.Domain.Interfaces.MQ;
using ParallelMarketing.Domain.Interfaces.Request;
using ParallelMarketing.Domain.Models;
using System.Text.Json;
using System.Runtime.CompilerServices;
using System.Threading.Channels;

namespace ParallelMarketing.Infrastructure.MQ
{
    public class MQCustomersConsumer : IMQCustomersConsumer
    {
        private readonly ILogger<MQCustomersConsumer> _logger;
        private readonly IRequestCustomersDomain _requestCustomersDomain;
        private readonly JsonSerializerOptions _options;

        public MQCustomersConsumer(ILogger<MQCustomersConsumer> logger, IRequestCustomersDomain requestCustomersDomain)
        {
            _logger = logger;
            _requestCustomersDomain = requestCustomersDomain;
            _options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
        }

        private static IModel CreateChannel(IConnection connection)
        {
            var channel = connection.CreateModel();
            return channel;
        }

        public async Task ConsumerMessageItau()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory { HostName = "localhost" };
                //var factory = new ConnectionFactory { HostName = "localhost", DispatchConsumersAsync = true };
                using (var connection = factory.CreateConnection())
                {
                    using (var channel = CreateChannel(connection))
                    {
                        //using (var channel = connection.CreateModel())
                        //{                    
                        channel.QueueDeclare(queue: "customers", durable: true, exclusive: false, autoDelete: false
                                        , arguments: new Dictionary<string, object>()
                                        {
                                            ["x-dead-letter-exchange"] = "DeadLetterQueue"
                                        });

                        channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                        var consumer = new EventingBasicConsumer(channel);

                        consumer.Received += (model, ea) =>
                        {
                            //try
                            //{
                                var body = ea.Body.ToArray();
                                var message = Encoding.UTF8.GetString(body);
                                var customer = JsonSerializer.Deserialize<Customers>(message, _options);
                                var result = _requestCustomersDomain.SendMarketingItau<Customers>(customer);

                            //    if (result != null)
                            //        channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                            //    else
                            //        channel.BasicNack(deliveryTag: ea.DeliveryTag, multiple: false, requeue: false);
                            //}
                            //catch (Exception)
                            //{
                            //    channel.BasicNack(deliveryTag: ea.DeliveryTag, multiple: false, requeue: true);
                            //}
                        };
                        channel.BasicConsume(queue: "customers", autoAck: true, consumer: consumer);

                    };
                };
            });
        }


        public async Task ConsumerMessageBradesco()
        {
            //await Task.Run(() =>
            //{
            var factory = new ConnectionFactory { HostName = "localhost" };
            //var factory = new ConnectionFactory() { Uri = new Uri("amqp://guest:guest@rabbitmq:5672/") };
            using var connection = factory.CreateConnection();
            using (var channel = connection.CreateModel())
            {
                //channel.ExchangeDeclare("bdo-customers-exh", "fanout");
                //channel.QueueDeclare("bdo-customers-dlq", true, false, false, null);
                //channel.QueueBind("bdo-customers-dlq", "bdo-customers-exh", "");

                //var args = new Dictionary<string, object>()
                //{
                //    { "x-dead-letter-exchange", "bdo-customers-exh" }
                //};

                channel.QueueDeclare(queue: "bdo-customers", durable: true, exclusive: false, autoDelete: false, arguments: null);
                channel.BasicQos(prefetchSize: 0, prefetchCount: 10, global: false);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    _logger.LogInformation($"Itaú Item: |{consumer}|.");
                    //try
                    //{
                    //    Random random = new Random();
                    //    var num = random.Next(1, 1000);
                    //    _logger.LogWarning($"Consumindo fila em {DateTimeOffset.Now}");

                    //    var body = ea.Body.ToArray();
                    //    var message = Encoding.UTF8.GetString(body);

                    //    if (num % 2 == 0)
                    //    {
                    //        _logger.LogWarning($"Número PAR: {num} - {DateTimeOffset.Now}");
                    //        channel.BasicAck(ea.DeliveryTag, false);
                    //    }
                    //    else
                    //    {
                    //        _logger.LogWarning($"Número ÍMPAR: {num} - {DateTimeOffset.Now}");
                    //        channel.BasicNack(ea.DeliveryTag, false, false);
                    //    }

                    //}
                    //catch (Exception ex)
                    //{
                    //    _logger.LogError($"--->> ERRO: |{ex.Message}|<<--- .");
                    //    channel.BasicNack(ea.DeliveryTag, false, false);
                    //}
                };

                channel.BasicConsume(queue: "bdo-customers", autoAck: false, consumer: consumer);
            };
            //});
        }

        public async Task ConsumerMessageNubank()
        {
            //await Task.Run(() =>
            //{
            var factory = new ConnectionFactory { HostName = "localhost" };
            //var factory = new ConnectionFactory() { Uri = new Uri("amqp://guest:guest@rabbitmq:5672/") };
            using var connection = factory.CreateConnection();
            using (var channel = connection.CreateModel())
            {
                //channel.ExchangeDeclare("nbk-customers-exh", "fanout");
                //channel.QueueDeclare("nbk-customers-dlq", true, false, false, null);
                //channel.QueueBind("nbk-customers-dlq", "nbk-customers-exh", "");

                //var args = new Dictionary<string, object>()
                //{
                //    { "x-dead-letter-exchange", "nbk-customers-exh" }
                //};

                channel.QueueDeclare(queue: "nbk-customers", durable: true, exclusive: false, autoDelete: false, arguments: null);

                channel.BasicQos(prefetchSize: 0, prefetchCount: 10, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    _logger.LogInformation($"Nubank Item: |{consumer}|.");
                    //try
                    //{
                    //    Random random = new Random();
                    //    var num = random.Next(1, 1000);
                    //    var body = ea.Body.ToArray();
                    //    var message = Encoding.UTF8.GetString(body);

                    //    if (num % 2 == 0)
                    //    {
                    //        _logger.LogWarning($"Número PAR: {num} - {DateTimeOffset.Now}");
                    //        channel.BasicAck(ea.DeliveryTag, false);
                    //    }
                    //    else
                    //    {
                    //        _logger.LogWarning($"Número ÍMPAR: {num} - {DateTimeOffset.Now}");
                    //        channel.BasicNack(ea.DeliveryTag, false, false);
                    //    }

                    //}
                    //catch (Exception ex)
                    //{
                    //    _logger.LogError($"--->> ERRO: |{ex.Message}|<<--- .");
                    //    channel.BasicNack(ea.DeliveryTag, false, false);
                    //}
                };
                channel.BasicConsume(queue: "nbk-customers", autoAck: true, consumer: consumer);
            };
            //});

        }

        public async Task ConsumerMessageSantander()
        {
            //await Task.Run(() =>
            //{
            //var factory = new ConnectionFactory() { Uri = new Uri("amqp://guest:guest@rabbitmq:5672/") };
            var factory = new ConnectionFactory { HostName = "localhost" };
            using var connection = factory.CreateConnection();
            using IModel channel = connection.CreateModel();

            //channel.ExchangeDeclare("std.customers.exc", ExchangeType.Fanout);
            //channel.QueueDeclare("std.customers.dlq", true, false, false, null);
            //channel.QueueBind("std.customers.dlq", "std.customers.exc", "");

            //var args = new Dictionary<string, object>()
            //        {
            //            { "x-dead-letter-exchange", "std.customers.exc" }
            //        };

            channel.QueueDeclare(queue: "std-customers", durable: true, exclusive: false, autoDelete: false, arguments: null);
            channel.BasicQos(prefetchSize: 0, prefetchCount: 10, global: false);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                //try
                //{
                _logger.LogInformation($"Santander Item: |{consumer}|.");
                //Random random = new Random();
                //var num =  random.Next(1, 10000);
                //var body = ea.Body.ToArray();
                //var message = Encoding.UTF8.GetString(body);

                //if (num > 0 && num <= 7000)
                //channel.BasicAck(ea.DeliveryTag, false);
                //else
                //    channel.BasicNack(ea.DeliveryTag, false, false);
                //}
                //catch (Exception ex)
                //{
                //    _logger.LogError($"{ex.Message}.");
                //    //channel.BasicNack(ea.DeliveryTag, false, true);
                //}
            };
            channel.BasicConsume(queue: "std-customers", autoAck: true, consumer: consumer);

            //});
        }

    }
}
