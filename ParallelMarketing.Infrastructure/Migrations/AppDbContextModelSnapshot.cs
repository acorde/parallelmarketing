﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using ParallelMarketing.Infrastructure.Context;

#nullable disable

namespace ParallelMarketing.Infrastructure.Migrations
{
    [DbContext(typeof(AppDbContext))]
    partial class AppDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "8.0.2")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            MySqlModelBuilderExtensions.AutoIncrementColumns(modelBuilder);

            modelBuilder.Entity("ParallelMarketing.Domain.Models.Address", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    MySqlPropertyBuilderExtensions.UseMySqlIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("Bairro")
                        .HasColumnType("longtext");

                    b.Property<string>("CEP")
                        .HasColumnType("longtext");

                    b.Property<string>("CodigoIbge")
                        .HasColumnType("longtext");

                    b.Property<string>("Complemento")
                        .HasColumnType("longtext");

                    b.Property<int>("CustomersId")
                        .HasColumnType("int");

                    b.Property<string>("Logradouro")
                        .HasColumnType("longtext");

                    b.Property<string>("Municipio")
                        .HasColumnType("longtext");

                    b.Property<string>("Numero")
                        .HasColumnType("longtext");

                    b.Property<string>("UF")
                        .HasColumnType("longtext");

                    b.Property<string>("UfParametro")
                        .HasColumnType("longtext")
                        .HasAnnotation("Relational:JsonPropertyName", "UfParametro");

                    b.HasKey("Id");

                    b.HasIndex("CustomersId")
                        .IsUnique();

                    b.ToTable("Address");
                });

            modelBuilder.Entity("ParallelMarketing.Domain.Models.Customers", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    MySqlPropertyBuilderExtensions.UseMySqlIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("ClientName")
                        .HasColumnType("longtext");

                    b.Property<string>("CnpjConsultado")
                        .HasColumnType("longtext");

                    b.Property<string>("CnpjParametro")
                        .HasColumnType("longtext");

                    b.Property<string>("CodigoHtml")
                        .HasColumnType("longtext");

                    b.Property<string>("CodigoInterno")
                        .HasColumnType("longtext");

                    b.Property<string>("InscricaoEstadual")
                        .HasColumnType("longtext");

                    b.Property<string>("NomeEmpresarial")
                        .HasColumnType("longtext");

                    b.Property<string>("NumeroInscricao")
                        .HasColumnType("longtext");

                    b.HasKey("Id");

                    b.ToTable("Customers");
                });

            modelBuilder.Entity("ParallelMarketing.Domain.Models.Address", b =>
                {
                    b.HasOne("ParallelMarketing.Domain.Models.Customers", null)
                        .WithOne("Address")
                        .HasForeignKey("ParallelMarketing.Domain.Models.Address", "CustomersId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("ParallelMarketing.Domain.Models.Customers", b =>
                {
                    b.Navigation("Address")
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
