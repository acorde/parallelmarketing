﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ParallelMarketing.Application.Interfaces;
using ParallelMarketing.Application.Services;
using ParallelMarketing.Domain.Interfaces;
using ParallelMarketing.Infrastructure.Context;
using ParallelMarketing.Infrastructure.MQ;
using ParallelMarketing.Infrastructure.Repository;
using ParallelMarketing.Infrastructure.Request;
using ParallelMarketing.Infrastructure;
using Microsoft.Extensions.Options;
using ParallelMarketing.Domain.Interfaces.Request;
using ParallelMarketing.Domain.Interfaces.MQ;

namespace ParallelMarketing.CrossCutting.AppDependencies
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            var mySqlConnection = configuration.GetConnectionString("DefaultConnection");
            //Registrar o serviço do contexto
            services.AddDbContext<AppDbContext>(options =>
                                                options.UseMySql(mySqlConnection,
                                                                 ServerVersion.AutoDetect(mySqlConnection)));
            services.AddScoped<ICustomersRepository, CustomersRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWorkRepository>();
            services.AddSingleton<IRequestCustomersDomain, RequestCustomers>();
            services.AddScoped<IRequestCustomersApplications, RequestCustomersApplicationServices>();
            services.AddScoped<IMQCustomersPublisher, MQCustomersPublisher>();
            services.AddSingleton<IMQCustomersConsumer, MQCustomersConsumer>();
            services.AddScoped(typeof(ICustomersApplicationServices<>), typeof(CustomersApplicationServices<>));            

            var myhandlers = AppDomain.CurrentDomain.Load("ParallelMarketing.Application");
            services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(myhandlers));

            return services;
        }

        public static void AddConfiguration<T>(this IServiceCollection services, IConfiguration configuration, string? configurationTag = null) where T : class
        {
            if (string.IsNullOrEmpty(configurationTag))
            {
                configurationTag = typeof(T).Name;
            }

            var instance = Activator.CreateInstance<T>();
            new ConfigureFromConfigurationOptions<T>(configuration.GetSection(configurationTag)).Configure(instance);
            services.AddSingleton(instance);
        }

        public static IServiceCollection ConfigureServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddConfiguration<SettingsConfiguration>(configuration, "MySettings");
            return services;
        }
    }
}
