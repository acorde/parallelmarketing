using Hangfire;
using Hangfire.Console;
using Hangfire.Redis.StackExchange;
using ParallelMarketing.Application.Mapper;
using ParallelMarketing.CrossCutting.AppDependencies;
using ParallelMarketing.Services.Hangfire;
using StackExchange.Redis;

var builder = WebApplication.CreateBuilder(args);
// Add services to the container.
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddInfrastructure(builder.Configuration);
builder.Services.ConfigureServices(builder.Configuration);

builder.Services.AddHangfire(options =>
{
    var connectionString = builder.Configuration.GetValue<string>("RedisConnection");
    var redis = ConnectionMultiplexer.Connect(connectionString);
    options.UseRedisStorage(redis, options: new RedisStorageOptions { Prefix = $"HANG_FIRE" });
    options.UseConsole();
});

builder.Services.AddHttpClient("parallel", HttpClient =>
{
    HttpClient.BaseAddress = new Uri("https://localhost:7041/");
});

builder.Services.AddHangfireServer();
builder.Services.AddHostedService<HFSchedullerServices>();
builder.Services.AddAutoMapper(typeof(ProfileMapper));
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();
app.MapHangfireDashboard("/hangfire");

//app.Start(HFCustomersServices);
app.Run();
