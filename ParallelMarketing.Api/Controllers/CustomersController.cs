﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using ParallelMarketing.Application.Interfaces;
using ParallelMarketing.Application.Customer.Command;
using ParallelMarketing.Domain.Interfaces.MQ;

namespace ParallelMarketing.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ILogger<CustomersController> _logger;
        private readonly IMediator _mediator;
        private readonly IMQCustomersPublisher _publisher;
        private readonly IMQCustomersConsumer _consumer;
        

        private readonly IRequestCustomersApplications _rquestCustomersApplication;

        public CustomersController(IMediator mediator
                                    , ILogger<CustomersController> logger
                                    , IMQCustomersPublisher publisher
                                    , IMQCustomersConsumer consumer
                                    //, IRequestCustomers requestCustomers
                                    , IMapper mapper
                                    , IRequestCustomersApplications rquestCustomersApplication)
        {
            _mediator = mediator;
            _logger = logger;
            _publisher = publisher;
            _consumer = consumer;
            //_requestCustomers = requestCustomers;
            _mapper = mapper;
            _rquestCustomersApplication = rquestCustomersApplication;
        }

        [HttpPost("Itau")]
        public async Task<IActionResult> AddCustomersItau(CreateCustomersCommand customer)
        {
            try
            {
                await _publisher.PublisherMessageItau(customer);
                var _customersDto = await _mediator.Send(customer);
                //var result = await _rquestCustomersApplication.RequestHttpItau(_customersDto);
                //var problemDetails = new ProblemDetails() { };
                if (_customersDto is null)
                    return BadRequest();
                else
                    return Ok(_customersDto);
            }
            catch (Exception ex)
            {
                _logger.LogError("Api Itaú - Ocorreu uma exceção: " + ex.Message);
                return new StatusCodeResult(500);
            }

        }

        //[HttpPost("Santander")]
        //public async Task<IActionResult> AddCustomersSantander(CreateCustomersCommand customer)
        //{
        //    try
        //    {
        //        await _publisher.PublisherMessageSantander(customer);
        //        var createCustomer = await _mediator.Send(customer);
        //        await _consumer.ConsumerMessageSantander();

        //        return Accepted(createCustomer);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError("Api Santander - Ocorreu uma exceção: " + ex.Message);
        //        return new StatusCodeResult(500);
        //    }

        //}

        //[HttpPost("Bradesco")]
        //public async Task<IActionResult> AddCustomersBradesco(CreateCustomersCommand customer)
        //{
        //    try
        //    {
        //        await _publisher.PublisherMessageBradesco(customer);
        //        var createCustomer = await _mediator.Send(customer);
        //        await _consumer.ConsumerMessageBradesco();
        //        return Accepted(createCustomer);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError("Api Bradesco - Ocorreu uma exceção: " + ex.Message);
        //        return new StatusCodeResult(500);
        //    }

        //}

        //[HttpPost("Nubank")]
        //public async Task<IActionResult> AddCustomersNubank(CreateCustomersCommand customer)
        //{
        //    try
        //    {
        //        await _publisher.PublisherMessageNubank(customer);
        //        var createCustomer = await _mediator.Send(customer);
        //        await _consumer.ConsumerMessageNubank();
        //        return Accepted(createCustomer);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError("Api Nubank - Ocorreu uma exceção: " + ex.Message);
        //        return new StatusCodeResult(500);
        //    }

        //}

    }
}
